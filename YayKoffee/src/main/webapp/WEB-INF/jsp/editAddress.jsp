<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Hello, world!</title>
</head>

<body>
<div class="container">
    <h2>Edit Location</h2>
    <form class="address" method="POST" action="${contextPath}/addressController/editAddress/${address.getId()}">
        <div class="form-group row">
            <label>Place Name</label>
            <input type="text" class="form-control" placeholder="Enter placeName"
                   name="placeName" value="${address.getPlaceName()}">
        </div>
        <div class="form-group row">
            <label>Address</label>
            <input type="text" class="form-control" placeholder="Enter address"
                   name="addressLocation" value="${address.getAddressLocation()}">
        </div>

        <div class="form-group row">
            <label>City</label>
            <input type="text" class="form-control" placeholder="Enter city" name="city" value="${address.getCity()}">
        </div>

        <div class="form-group row">
            <label>Province</label>
            <input type="text" class="form-control" placeholder="Enter province"
                   name="province" value="${address.getProvince()}">
        </div>

        <div class="form-group row">
            <label>District</label>
            <input type="text" class="form-control" placeholder="Enter district"
                   name="district" value="${address.getDistrict()}">
        </div>

        <div class="form-group row">
            <label>Phone Number</label>
            <input type="text" class="form-control" placeholder="Enter phoneNumber"
                   name="phoneNumber" value="${address.getPhoneNumber()}">
        </div>
        <div class="form-group row">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>

</body>

</html>