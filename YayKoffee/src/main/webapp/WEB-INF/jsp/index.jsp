<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Dashboard Admin Yay!Koffee</title>
</head>

<body>
<div class="container">
    <h1 class="text-center">Dashboard Admin</h1>
    <c:if test="${message != null}">
        <div class="alert alert-success text-center" role="alert">
                ${message}
        </div>
    </c:if>
    <c:if test="${messageDel != null}">
        <div class="alert alert-danger text-center" role="alert">
                ${messageDel}
        </div>
    </c:if>
    <ul class="nav nav-tabs">
        <li class=" ${className == 'home' ? 'active' : ''}"><a data-toggle="tab" href="#home">Home & Blog</a></li>
        <li class=" ${className == 'location' ? 'active' : ''} "><a data-toggle="tab" href="#menu1">Location</a></li>
        <li class=" ${className == 'menu' ? 'active' : ''} "><a data-toggle="tab" href="#menu2">Menu</a></li>
        <li class=" ${className == 'about' ? 'active' : ''} "><a data-toggle="tab" href="#menu3">About Us</a></li>
    </ul>

    <div class="tab-content">
        <div id="home" class="${className == 'home' ? 'tab-pane fade in active' : 'tab-pane fade'}">
            <!-- Tab Add Home & Blog Start -->
            <div class="container">
                <h2>Add Home & Blog</h2>
                <form class="sections" method="POST" action="${contextPath}/sectionController">
                    <div class="form-group row">
                        <label>Image URL</label>
                        <input type="text" class="form-control" placeholder="Enter imageUrl"
                               name="imageUrl">
                    </div>
                    <div class="form-group row">
                        <label>Title</label>
                        <input type="text" class="form-control" placeholder="Enter title" name="title">
                    </div>

                    <div class="form-group row">
                        <label>Description</label>
                        <textarea class="form-control" aria-label="With textarea" rows="5" class="form-control"
                                  placeholder="Enter description" name="description"></textarea>
                    </div>

                    <div class="form-group row">
                        <label>Date Post</label>
                        <input type="date" class="form-control" placeholder="Enter datePost"
                               name="datePost">
                    </div>
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                    </div>
                </form>

                <hr>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Title</th>
                        <th scope="col">Description</th>
                        <th scope="col">Date Post</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${sections}" var="sections">
                        <tr>
                            <th scope="row">${sections.getId()}</th>
                            <td>${sections.getTitle()}</td>
                            <td>${sections.getDescription()}</td>
                            <td>${sections.getDatePost()}</td>
                            <td>
                                <a type="button" href="${contextPath}/editSections/${sections.getId()}"
                                   class="btn btn-success btn-sm pd-3">Edit</a>
                                <a type="button" onclick="return confirm('Are you sure?');"
                                   href="${contextPath}/deleteSection/${sections.getId()}"
                                   class="btn btn-danger btn-sm">Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <!-- Tab Add Home & Blog End -->
        </div>

        <div id="menu1" class="${className == 'location' ? 'tab-pane fade in active' : 'tab-pane fade'}">
            <!-- Tab Add Location Start -->
            <div class="container">
                <h2>Add Location</h2>
                <form class="address" method="POST" action="${contextPath}/addressController">
                    <div class="form-group row">
                        <label>Place Name</label>
                        <input type="text" class="form-control" placeholder="Enter placeName"
                               name="placeName">
                    </div>
                    <div class="form-group row">
                        <label>Address</label>
                        <input type="text" class="form-control" placeholder="Enter address"
                               name="addressLocation">
                    </div>

                    <div class="form-group row">
                        <label>City</label>
                        <input type="text" class="form-control" placeholder="Enter city" name="city">
                    </div>

                    <div class="form-group row">
                        <label>Province</label>
                        <input type="text" class="form-control" placeholder="Enter province"
                               name="province">
                    </div>

                    <div class="form-group row">
                        <label>District</label>
                        <input type="text" class="form-control" placeholder="Enter district"
                               name="district">
                    </div>

                    <div class="form-group row">
                        <label>Phone Number</label>
                        <input type="text" class="form-control" placeholder="Enter phoneNumber"
                               name="phoneNumber">
                    </div>
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>

                <hr>
                <table class="table table-responsive-lg">
                    <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Place Name</th>
                        <th scope="col">Address</th>
                        <th scope="col">Province</th>
                        <th scope="col">City</th>
                        <th scope="col">District</th>
                        <th scope="col">Phone Number</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${address}" var="address">
                        <tr>
                            <th scope="row">${address.getId()}</th>
                            <td>${address.getPlaceName()}</td>
                            <td>${address.getAddressLocation()}</td>
                            <td>${address.getProvince()}</td>
                            <td>${address.getCity()}</td>
                            <td>${address.getDistrict()}</td>
                            <td>${address.getPhoneNumber()}</td>
                            <td>
                                <a type="button" href="${contextPath}/addressController/editAddress/${address.getId()}"
                                   class="btn btn-success btn-sm pd-3">Edit</a>
                                <a type="button" onclick="return confirm('Are you sure?');"
                                   href="${contextPath}/deleteAddress/${address.getId()}"
                                   class="btn btn-danger btn-sm">Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <!-- Tab Add Location End -->
        </div>
        <div id="menu2" class="${className == 'menu' ? 'tab-pane fade in active' : 'tab-pane fade'}">
            <!-- Tab Add Menu Start -->
            <div class="container">
                <h2>Add Menu</h2>
                <form class="menu" method="POST" action="${contextPath}/">
                    <div class="form-group row">
                        <label>Image URL</label>
                        <input type="text" class="form-control" placeholder="Enter imageUrl"
                               name="imageUrl">
                    </div>

                    <div class="form-group row">
                        <label>Title</label>
                        <input type="text" class="form-control" placeholder="Enter title" name="title">
                    </div>

                    <div class="form-group row">
                        <label>Price</label>
                        <input type="text" class="form-control" placeholder="Enter price" name="price">
                    </div>
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>

                <hr>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Image URL</th>
                        <th scope="col">Title</th>
                        <th scope="col">Price</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${menu}" var="menu">
                        <tr>
                            <th scope="row">${menu.getId()}</th>
                            <td>${menu.getImageUrl()}</td>
                            <td>${menu.getTitle()}</td>
                            <td>${menu.getPrice()}</td>
                            <td>
                                <a type="button" href="${contextPath}/editMenu/${menu.getId()}"
                                   class="btn btn-success btn-sm pd-3">Edit</a>
                                <a type="button" onclick="return confirm('Are you sure?');"
                                   href="${contextPath}/deleteMenu/${menu.getId()}"
                                   class="btn btn-danger btn-sm">Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <!-- Tab Add Menu End -->
        </div>
        <div id="menu3" class="${className == 'about' ? 'tab-pane fade in active' : 'tab-pane fade'}">
            <!-- Tab Add About Us Start -->
            <div class="container">
                <h2>Add About Us</h2>
                <form class="about" method="POST" action="${contextPath}/aboutController">

                    <div class="form-group row">
                        <label>Title</label>
                        <input type="text" class="form-control" placeholder="Enter title" name="title">
                    </div>

                    <div class="form-group row">
                        <label>Description</label>
                        <textarea class="form-control" aria-label="With textarea" rows="5" class="form-control"
                                  placeholder="Enter description" name="description"></textarea>
                    </div>
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>

                <hr>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Title</th>
                        <th scope="col">Description</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${about}" var="about">
                        <tr>
                            <th scope="row">${about.getId()}</th>
                            <td>${about.getTitle()}</td>
                            <td>${about.getDescription()}</td>
                            <td>
                                <a type="button" href="${contextPath}/aboutController/editAbout/${about.getId()}"
                                   class="btn btn-success btn-sm pd-3">Edit</a>
                                <a type="button" onclick="return confirm('Are you sure?');"
                                   href="${contextPath}/deleteAboutController/${about.getId()}"
                                   class="btn btn-danger btn-sm">Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <!-- Tab Add About Us End -->
        </div>

    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>

<%--<script>--%>
<%--    function deleteSection(){--%>
<%--        var result = confirm("Want to delete?");--%>
<%--        if (result) {--%>

<%--        }--%>
<%--    }--%>
<%--</script>--%>

</body>

</html>