package id.co.nexsoft.yaykoffee.api;

import id.co.nexsoft.yaykoffee.model.Menu;
import id.co.nexsoft.yaykoffee.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin()
public class MenuApi {
	@Autowired
	private MenuService menuService;
	
	@GetMapping("/menu")
	public List<Menu> getALlMenu() {
		return menuService.getAllMenu();
	}
	
	@GetMapping("/menu/{id}")
	public Menu getMenuById(int id) {
		return menuService.getMenuById(id);
	}
}
