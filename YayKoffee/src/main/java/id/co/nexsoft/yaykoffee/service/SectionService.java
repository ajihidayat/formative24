package id.co.nexsoft.yaykoffee.service;

import id.co.nexsoft.yaykoffee.model.Section;
import id.co.nexsoft.yaykoffee.repository.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SectionService {
	@Autowired
	private SectionRepository sectionRepository;
	
	public List<Section> getAllSection() {
		return sectionRepository.findAll();
	}
	
	public Section getSectionById(int id) {
		return sectionRepository.findById(id);
	}
	
	public void deleteSectionById(int id) {
		sectionRepository.deleteById(id);
	}
	
	public void insertSection(Section section) {
		sectionRepository.save(section);
	}
	
	public String updateSection(Section section, int id) {
		Section findSection = sectionRepository.findById(id);
		
		if (findSection == null) {
			return "tidak ditemukan";
		}
		
		section.setId(id);
		sectionRepository.save(section);
		return "Success Update";
	}
}
