package id.co.nexsoft.yaykoffee.controller;

import id.co.nexsoft.yaykoffee.model.Section;
import id.co.nexsoft.yaykoffee.service.SectionService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class SectionController {
	private final SectionService sectionService;
	private final HomeController homeController;
	
	public SectionController(SectionService sectionService, HomeController homeController) {
		this.sectionService = sectionService;
		this.homeController = homeController;
	}
	
	@PostMapping(value = "/sectionController")
	@ResponseStatus(HttpStatus.CREATED)
	public String addSection(Section section, Model model) {
		sectionService.insertSection(section);
		model.addAttribute("className", "home");
		model.addAttribute("message", "Insert Section Successfully");
		return homeController.getHome(model);
	}
	
	@GetMapping(value = "editSections/{id}")
	public String editSection(@PathVariable int id, Model model) {
		model.addAttribute("section", sectionService.getSectionById(id));
		return "editSection";
	}
	
	@PostMapping(value = "sectionController/{id}")
	public String updateSection(@PathVariable int id, Section section, Model model) {
		sectionService.updateSection(section, id);
		model.addAttribute("className", "home");
		model.addAttribute("message", "Update Section Successfully");
		return homeController.getHome(model);
	}
	
	@GetMapping(value = "/deleteSection/{id}")
	public String deleteSection(@PathVariable int id, Model model) {
		sectionService.deleteSectionById(id);
		model.addAttribute("className", "home");
		model.addAttribute("messageDel", "Delete Section Successfully");
		return homeController.getHome(model);
	}
}
