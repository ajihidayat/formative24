package id.co.nexsoft.yaykoffee.service;

import id.co.nexsoft.yaykoffee.model.Address;
import id.co.nexsoft.yaykoffee.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService {
	@Autowired
	AddressRepository addressRepository;
	
	public List<Address> getAllAddress() {
		return addressRepository.findAll();
	}
	
	public Address getAddress(int id) {
		return addressRepository.findById(id);
	}
	
	public void saveAddress(Address address) {
		addressRepository.save(address);
	}
	
	public String updateAddress(int id, Address address) {
		Address address1 = addressRepository.findById(id);
		if (address1 == null) {
			return "tidak ditemukan";
		}
		address.setId(id);
		addressRepository.save(address);
		return "Address Successfully";
	}
	
	public void deleteAddress(int id) {
		addressRepository.deleteById(id);
	}
}
