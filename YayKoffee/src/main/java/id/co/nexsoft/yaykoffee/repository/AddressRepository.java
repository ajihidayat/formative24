package id.co.nexsoft.yaykoffee.repository;

import id.co.nexsoft.yaykoffee.model.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer> {
	
	List<Address> findAll();
	
	Address findById(int id);
	
	Address save(Address address);
	
	void deleteById(int id);
}