package id.co.nexsoft.yaykoffee.repository;

import id.co.nexsoft.yaykoffee.model.Menu;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuRepository extends CrudRepository<Menu, Integer> {
	Menu findById(int id);
	
	List<Menu> findAll();
	
	void deleteById(int id);
}
