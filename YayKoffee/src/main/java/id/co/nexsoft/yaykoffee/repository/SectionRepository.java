package id.co.nexsoft.yaykoffee.repository;

import id.co.nexsoft.yaykoffee.model.Section;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SectionRepository extends CrudRepository<Section, Integer> {
	Section findById(int id);
	
	@Query(value = "SELECT * FROM section ORDER BY date_post DESC",
			nativeQuery = true)
	List<Section> findAll();
	
	void deleteById(int id);
}
