package id.co.nexsoft.yaykoffee.api;

import id.co.nexsoft.yaykoffee.model.Address;
import id.co.nexsoft.yaykoffee.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin()
public class AddressApi {
	@Autowired
	AddressService addressService;
	
	@GetMapping("/addressApi")
	public List<Address> getAllAddress() {
		return addressService.getAllAddress();
	}
	
	@GetMapping("/addressApi/{id}")
	public Address getAddressById(@PathVariable int id) {
		return addressService.getAddress(id);
	}
}
