package id.co.nexsoft.yaykoffee.service;

import id.co.nexsoft.yaykoffee.model.About;
import id.co.nexsoft.yaykoffee.repository.AboutRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AboutService {
	@Autowired
	AboutRepository aboutRepository;
	
	public List<About> getAllAbout() {
		return aboutRepository.findAll();
	}
	
	public About getAboutById(int id) {
		return aboutRepository.findById(id);
	}
	
	public void saveAbout(About about) {
		aboutRepository.save(about);
	}
	
	public void deleteAboutById(int id) {
		aboutRepository.delete(aboutRepository.findById(id));
	}
	
	public String updateAbout(int id, About about) {
		About about1 = aboutRepository.findById(id);
		if (about1 == null) {
			return "tidak ditemukan";
		}
		about.setId(id);
		aboutRepository.save(about);
		return "About Successfully";
	}
	
	
}
