package id.co.nexsoft.yaykoffee.api;

import id.co.nexsoft.yaykoffee.model.About;
import id.co.nexsoft.yaykoffee.service.AboutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin()
public class AboutApi {
	@Autowired
	AboutService aboutService;
	
	@GetMapping(value = "/aboutApi")
	public List<About> getAllAbout() {
		return aboutService.getAllAbout();
	}
	
	@GetMapping(value = "aboutApi/{id}")
	public About getAboutById(int id) {
		return aboutService.getAboutById(id);
	}
}
