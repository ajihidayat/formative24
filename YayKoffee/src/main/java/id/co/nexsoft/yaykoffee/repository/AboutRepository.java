package id.co.nexsoft.yaykoffee.repository;

import id.co.nexsoft.yaykoffee.model.About;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AboutRepository extends CrudRepository<About, Integer> {
	
	List<About> findAll();
	
	About findById(int id);
	
	About save(About about);
	
	void delete(About about);
	
}