package id.co.nexsoft.yaykoffee.model;

import javax.persistence.*;

@Entity
public class About {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id;
	private String title;
	@Column(columnDefinition = "TEXT")
	private String description;
	
	public About(String title, String description) {
		this.title = title;
		this.description = description;
	}
	
	public About() {
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
}
