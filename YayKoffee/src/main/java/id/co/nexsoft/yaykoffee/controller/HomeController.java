package id.co.nexsoft.yaykoffee.controller;

import id.co.nexsoft.yaykoffee.service.AboutService;
import id.co.nexsoft.yaykoffee.service.AddressService;
import id.co.nexsoft.yaykoffee.service.MenuService;
import id.co.nexsoft.yaykoffee.service.SectionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController {
	private final SectionService sectionService;
	private final MenuService menuService;
	private final AddressService addressService;
	private final AboutService aboutService;
	
	public HomeController(SectionService sectionService, MenuService menuService, AddressService addressService, AboutService aboutService) {
		this.sectionService = sectionService;
		this.menuService = menuService;
		this.addressService = addressService;
		this.aboutService = aboutService;
	}
	
	@GetMapping("/")
	public String getHome(Model model) {
		model.addAttribute("sections", sectionService.getAllSection());
		model.addAttribute("about", aboutService.getAllAbout());
		model.addAttribute("address", addressService.getAllAddress());
		model.addAttribute("menu", menuService.getAllMenu());
		if (model.getAttribute("className") == null) {
			model.addAttribute("className", "home");
		}
		return "index";
	}
	
}
