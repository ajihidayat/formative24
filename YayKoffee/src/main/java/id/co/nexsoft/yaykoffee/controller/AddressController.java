package id.co.nexsoft.yaykoffee.controller;

import id.co.nexsoft.yaykoffee.model.Address;
import id.co.nexsoft.yaykoffee.service.AddressService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AddressController {
	private final AddressService addressService;
	private final HomeController homeController;
	
	public AddressController(AddressService addressService, HomeController homeController) {
		this.addressService = addressService;
		this.homeController = homeController;
	}
	
	
	@PostMapping(value = "/addressController")
	public String addAddress(Address address, Model model) {
		addressService.saveAddress(address);
		model.addAttribute("className", "location");
		model.addAttribute("message", "Insert Address Successfully");
		return homeController.getHome(model);
	}
	
	@GetMapping(value = "addressController/editAddress/{id}")
	public String editAddress(@PathVariable int id, Model model) {
		model.addAttribute("address", addressService.getAddress(id));
		model.addAttribute("className", "location");
		return "editAddress";
	}
	
	@PostMapping(value = "addressController/editAddress/{id}")
	public String updateAddress(@PathVariable int id, Address address, Model model) {
		addressService.updateAddress(id, address);
		model.addAttribute("className", "location");
		model.addAttribute("message", "Update Address Successfully");
		return homeController.getHome(model);
	}
	
	@GetMapping(value = "/deleteAddress/{id}")
	public String deleteAddress(@PathVariable int id, Model model) {
		addressService.deleteAddress(id);
		model.addAttribute("className", "location");
		model.addAttribute("messageDel", "Delete Address Successfully");
		return homeController.getHome(model);
	}
}
