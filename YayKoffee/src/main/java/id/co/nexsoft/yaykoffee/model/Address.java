package id.co.nexsoft.yaykoffee.model;

import javax.persistence.*;

@Entity
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id;
	private String placeName;
	private String addressLocation;
	private String city;
	private String province;
	private String district;
	private String phoneNumber;
	
	public Address(String placeName, String addressLocation, String city, String province, String district, String phoneNumber) {
		this.placeName = placeName;
		this.addressLocation = addressLocation;
		this.city = city;
		this.province = province;
		this.district = district;
		this.phoneNumber = phoneNumber;
	}
	
	public Address() {
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getPlaceName() {
		return placeName;
	}
	
	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}
	
	public String getAddressLocation() {
		return addressLocation;
	}
	
	public void setAddressLocation(String address) {
		this.addressLocation = address;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getProvince() {
		return province;
	}
	
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getDistrict() {
		return district;
	}
	
	public void setDistrict(String district) {
		this.district = district;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@Override
	public String toString() {
		return "Address{" +
				       "id=" + id +
				       ", placeName='" + placeName + '\'' +
				       ", address='" + addressLocation + '\'' +
				       ", city='" + city + '\'' +
				       ", province='" + province + '\'' +
				       ", district='" + district + '\'' +
				       ", phoneNumber='" + phoneNumber + '\'' +
				       '}';
	}
}
