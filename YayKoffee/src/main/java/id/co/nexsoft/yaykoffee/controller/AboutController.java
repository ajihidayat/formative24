package id.co.nexsoft.yaykoffee.controller;

import id.co.nexsoft.yaykoffee.model.About;
import id.co.nexsoft.yaykoffee.service.AboutService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class AboutController {
	private final AboutService aboutService;
	private final HomeController homeController;
	
	public AboutController(AboutService aboutService, HomeController homeController) {
		this.aboutService = aboutService;
		this.homeController = homeController;
	}
	
	@PostMapping("/aboutController")
	public String addAbout(About about, Model model) {
		aboutService.saveAbout(about);
		model.addAttribute("className", "about");
		model.addAttribute("message", "Insert About Us Successfully");
		
		return homeController.getHome(model);
	}
	
	@GetMapping(value = "aboutController/editAbout/{id}")
	public String editAbout(@PathVariable int id, Model model) {
		model.addAttribute("about", aboutService.getAboutById(id));
		return "editAbout";
	}
	
	@PostMapping(value = "aboutController/editAbout/{id}")
	public String updateAbout(@PathVariable int id, About about, Model model) {
		aboutService.updateAbout(id, about);
		model.addAttribute("className", "about");
		model.addAttribute("message", "Update About Us Successfully");
		
		return homeController.getHome(model);
	}
	
	
	@GetMapping(value = "/deleteAboutController/{id}")
	public String deleteAbout(@PathVariable int id, Model model) {
		aboutService.deleteAboutById(id);
		model.addAttribute("className", "about");
		model.addAttribute("messageDel", "Delete About Us Successfully");
		return homeController.getHome(model);
	}
}
