package id.co.nexsoft.yaykoffee.service;

import id.co.nexsoft.yaykoffee.model.Menu;
import id.co.nexsoft.yaykoffee.repository.MenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuService {
	@Autowired
	private MenuRepository menuRepository;
	
	public List<Menu> getAllMenu() {
		return menuRepository.findAll();
	}
	
	public Menu getMenuById(int id) {
		return menuRepository.findById(id);
	}
	
	public void deleteMenuById(int id) {
		menuRepository.deleteById(id);
	}
	
	public void insertMenu(Menu menu) {
		menuRepository.save(menu);
	}
	
	public String updateMenu(Menu menu, int id) {
		Menu findMenu = menuRepository.findById(id);
		
		if (findMenu == null) {
			return "tidak ditemukan";
		}
		
		menu.setId(id);
		menuRepository.save(menu);
		return "Success Update";
	}
}
