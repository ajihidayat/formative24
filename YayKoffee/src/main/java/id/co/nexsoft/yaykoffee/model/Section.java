package id.co.nexsoft.yaykoffee.model;

import javax.persistence.*;

@Entity

public class Section {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id;
	@Column(columnDefinition = "TEXT")
	private String imageUrl;
	private String title;
	@Column(columnDefinition = "TEXT")
	private String description;
	@Column(columnDefinition = "TEXT")
	private String datePost;
	
	public Section() {
	}
	
	public Section(int id, String imageUrl, String title, String description, String datePost) {
		this.id = id;
		this.imageUrl = imageUrl;
		this.title = title;
		this.description = description;
		this.datePost = datePost;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}
	
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDatePost() {
		return datePost;
	}
	
	public void setDatePost(String datePost) {
		this.datePost = datePost;
	}
}
