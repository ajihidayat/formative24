package id.co.nexsoft.yaykoffee.api;

import id.co.nexsoft.yaykoffee.model.Section;
import id.co.nexsoft.yaykoffee.service.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin()
public class SectionApi {
	@Autowired
	private SectionService sectionService;
	
	@GetMapping("/section")
	public List<Section> getAllSection() {
		return sectionService.getAllSection();
	}
	
	@GetMapping("/section/{id}")
	public Section getScetionById(int id) {
		return sectionService.getSectionById(id);
	}
}
