package id.co.nexsoft.yaykoffee.controller;

import id.co.nexsoft.yaykoffee.model.Menu;
import id.co.nexsoft.yaykoffee.service.MenuService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class MenuController {
	private final MenuService menuService;
	private final HomeController homeController;
	
	public MenuController(MenuService menuService, HomeController homeController) {
		this.menuService = menuService;
		this.homeController = homeController;
	}
	
	@PostMapping(value = "/")
	@ResponseStatus(HttpStatus.CREATED)
	public String addMenu(Menu menu, Model model) {
		menuService.insertMenu(menu);
		model.addAttribute("className", "menu");
		model.addAttribute("message", "Insert Menu Successfully");
		return homeController.getHome(model);
	}
	
	@GetMapping(value = "/editMenu/{id}")
	public String editMenu(@PathVariable int id, Model model) {
		model.addAttribute("menu", menuService.getMenuById(id));
		return "editMenu";
	}
	
	@PostMapping(value = "/editMenu/{id}")
	public String updateMenu(@PathVariable int id, Menu menu, Model model) {
		menuService.updateMenu(menu, id);
		model.addAttribute("className", "menu");
		model.addAttribute("message", "Update Menu Successfully");
		return homeController.getHome(model);
	}
	
	@GetMapping(value = "/deleteMenu/{id}")
	public String deleteMenu(@PathVariable int id, Model model) {
		System.out.println("waw");
		menuService.deleteMenuById(id);
		model.addAttribute("className", "menu");
		model.addAttribute("messageDel", "Delete Menu Successfully");
		return homeController.getHome(model);
	}
}
