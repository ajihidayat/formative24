package id.co.nexsoft.yaykoffee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YayKoffeeApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(YayKoffeeApplication.class, args);
	}
	
}
