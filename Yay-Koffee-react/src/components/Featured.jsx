import React from 'react';
import { Link } from 'react-router-dom'

export default function Featured(props) {
    const { menu } = props;
    return (
        <div id="featured">
            <span className="whatshot"><Link to="/menu">Find out more</Link></span>
            <div>
                {
                    menu.slice(0, 2).map(item => {
                        const { imageUrl } = item;
                        return (
                            <a href="#"><img src={imageUrl} alt="Image" /></a>
                        )
                    })
                }
            </div>
        </div>
    )
}