import React from 'react';
import { Link, useLocation } from 'react-router-dom'

export default function Navbar() {
	const location = useLocation();
	const { pathname } = location;
	const splitLocation = pathname.split("/")

	return (
		<div id="header">
			<a href="index.html"><img src="images/logo.png" alt="Image" /></a>
			<ul>
				<li className={splitLocation[1] === "" ? "current" : ""}>
					<a href="#">
						<Link to="/">Home</Link>
					</a>
				</li>
				<li className={splitLocation[1] === "menu" ? "current" : ""}>
					<a href="#">
						<Link to="/menu">Menu</Link>
					</a>
				</li>
				<li className={splitLocation[1] === "location" ? "current" : ""}>
					<a href="#">
						<Link to="/location">Location</Link>
					</a>
				</li>
				<li className={splitLocation[1] === "blog" ? "current" : ""}>
					<a href="#">
						<Link to="/blog">Blog</Link>
					</a>
				</li>
				<li className={splitLocation[1] === "aboutUs" ? "current" : ""}>
					<a href="#">
						<Link to="/aboutUs">About Us</Link>
					</a>
				</li>
			</ul>
		</div>
	)
}