import React from 'react';

export default function BlogContent(props) {
    const { datePost, description, title, imageUrl, id, onCLick } = props;

    let getMonth = (month) => {
        let monthNames = ["Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec"];

        return monthNames[month];
    }

    const styleContent = { textAlign: 'justify' }
    const styleHref = { cursor: 'pointer', marginTop: '5px' }

    const date = new Date(datePost);
    let day = '' + date.getDate();
    const month = getMonth(date.getMonth());
    const year = date.getFullYear();
    if (day.length < 2)
        day = '0' + day;

    return (
        <li key={id}>
            <div>
                <div>
                    <span class="month">{month} {year}</span><span class="date">{day}</span>
                </div>
                <h1><a href="/blog">{title}</a></h1>
            </div>
            <div>
                <img src={imageUrl} alt={title} />
            </div>
            <p style={styleContent}>
                {description}
            </p>
            <a onClick={onCLick} style={styleHref} class="readmore">Back</a>
        </li>
    )
}