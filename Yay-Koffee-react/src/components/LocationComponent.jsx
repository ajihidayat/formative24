import React from 'react';

export default function LocationComponent(props) {
    const { location } = props

    return (
        <div>
            <a href="locations.html" className="locations">Locations</a>
            <div>
                {
                    location.map(item => {
                        const { placeName, addressLocation, city, province, district, phoneNumber } = item;
                        return (
                            <dl>
                                <dt>{placeName}</dt>
                                <dd>{addressLocation}</dd>
                                <dd>{district}</dd>
                                <dd>{city}</dd>
                                <dd>{province}</dd>
                                <dd>{phoneNumber}</dd>
                            </dl>
                        )
                    })
                }
            </div>
        </div>
    )
}