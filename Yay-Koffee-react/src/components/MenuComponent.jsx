import React from 'react';

export default function MenuComponent(props) {
    const { menu } = props
    return (
        <div>
            <a href="menu.html" className="whatshot">What's Hot</a>
            <div>
                <ul>
                    {
                        menu.map(item => {
                            const { imageUrl, title, price } = item;
                            return (
                                <li>
                                    <a href="index.html"><img src={imageUrl} alt="Image" className="menu-image" /></a>
                                    <div>
                                        <a href="index.html">{title}</a>
                                        <p>
                                            ${price}
                                        </p>
                                    </div>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        </div>
    )
}