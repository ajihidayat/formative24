import React from 'react';

export default function Header(props) {
    const { image, idHeader, text, aHref, aText } = props;

    return (
        <div id="figure">
            <img src={image} alt="Image" />
            <span id={idHeader}>{text} <a href={aHref} target="_blank">{aText}</a></span>
        </div>
    )
}