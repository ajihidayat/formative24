import React from 'react';

export default function AboutUsComponent(props) {
    const { about } = props;

    return (
        <div>
            <a href="about.html" className="about">About</a>
            <div>
                {
                    about.map(item => {
                        const { title, description } = item;
                        return (
                            <>
                                <h3>{title}</h3>
                                <p>
                                    {description}
                                </p>
                            </>
                        )
                    })
                }
            </div>
        </div>
    )
}