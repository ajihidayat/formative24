import React from 'react';
import { Link, useLocation } from 'react-router-dom'

export default function Footer() {
	const location = useLocation();
	const { pathname } = location;
	const splitLocation = pathname.split("/")

	return (
		<div id="footer">
			<div>
				<a href="index.html"><img src="images/logo2.png" alt="Image" /></a>
				<p className="footnote">
					Lorem ipsum dolor sit amet consectetur adipisicing elit.
				</p>
			</div>
			<div className="section">
				<ul>
					<li className={splitLocation[1] === "" ? "current" : ""}>
						<a href="#">
							<Link to="/">Home</Link>
						</a>
					</li>
					<li className={splitLocation[1] === "menu" ? "current" : ""}>
						<a href="#">
							<Link to="/menu">Menu</Link>
						</a>
					</li>
					<li className={splitLocation[1] === "location" ? "current" : ""}>
						<a href="#">
							<Link to="/location">Location</Link>
						</a>
					</li>
					<li className={splitLocation[1] === "blog" ? "current" : ""}>
						<a href="#">
							<Link to="/blog">Blog</Link>
						</a>
					</li>
					<li className={splitLocation[1] === "aboutUs" ? "current" : ""}>
						<a href="#">
							<Link to="/aboutUs">About Us</Link>
						</a>
					</li>
				</ul>
				<div id="connect">
					<a href="http://www.facebook.com" target="_blank" id="facebook">Facebook</a>
					<a href="http://www.twitter.com" target="_blank" id="twitter">Twitter</a>
					<a href="http://www.google.com" target="_blank" id="googleplus">Google+</a>
					<a href="index.html" id="rss">RSS</a>
				</div>
				<p>
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt rem alias deserunt accusantium,
					dignissimos esse, sequi, officia cum reiciendis eveniet voluptas eaque consectetur? Quae impedit
					facilis aliquam quis at sint?
				</p>
			</div>
		</div>
	)
}