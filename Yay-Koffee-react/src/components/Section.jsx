import React, { useState } from 'react';

export default function Section(props) {
    const { section } = props;

    return (
        <div className="section">
            <ul>
                {
                    section.slice(0, 3).map((item) => {
                        const { imageUrl, title, description } = item;
                        return (
                            <li>
                                <a href="/blog"><img src={imageUrl} alt={title} /></a>
                                <h2><a href="/blog">{title}</a></h2>
                                <p>
                                    {description.substring(0, 120)}...
                                </p>
                                <a href="/blog" class="readmore">Read More</a>
                            </li>
                        )
                    })
                }
            </ul>
            <div>
                <ul>
                    {
                        section.slice(0, 2).map((item) => {
                            const { datePost, title, description } = item;
                            return (
                                <li>
                                    <h3><a href="/blog">{title}</a></h3>
                                    <span>{datePost}</span>
                                    <p>
                                        {description.substring(0, 50)}...
                                    </p>
                                    <a href="/blog" class="readmore">Read more</a>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>

        </div>
    )
}