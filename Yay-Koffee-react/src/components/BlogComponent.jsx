import React, { useState } from 'react';
import BlogContent from './BlogContent';
import ReactPaginate from 'react-paginate';
import { useEffect } from 'react';

export default function BlogComponent(props) {
    const { blog } = props;
    const [currentPage, setCurrentPage] = useState(0);

    const [show, setShow] = useState(false)
    const [idBlog, setIdBlog] = useState(0);
    const [dPost, setDPost] = useState("");
    const [titleBlog, setTitleBlog] = useState("");
    const [desBlog, setDesBlog] = useState("");
    const [imageBlog, setImageBlog] = useState("");

    let getMonth = (month) => {
        let monthNames = ["Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec"];

        return monthNames[month];
    }

    const expand = (item) => {
        setShow(true);
        setIdBlog(item.id);
        setTitleBlog(item.title);
        setDesBlog(item.description);
        setDPost(item.datePost);
        setImageBlog(item.imageUrl);
    }

    

    //pagination
    let handlePageClick = ({ selected: selectedPage }) => {
        setCurrentPage(selectedPage);
    }
    const close = () => {
        setShow(false);
        setIdBlog(0);
        setTitleBlog("");
        setDesBlog("");
        setDPost("");
        setImageBlog("");
        setCurrentPage(0);
    }
    const PER_PAGE = 3;
    const offset = currentPage * PER_PAGE;
    const currentPageData = blog.slice(offset, offset + PER_PAGE);
    const pageCount = Math.ceil(blog.length / PER_PAGE);

    useEffect(() => {
        window.scrollTo({ behavior: 'smooth', top: '0px' });
    }, [currentPage, <BlogComponent />])

    return (
        <div>
            <a href="/blog" className="blog">Blog</a>
            <div id="blog">
                <ul>
                    {
                        (show === true)
                            ? <BlogContent id={idBlog} datePost={dPost} title={titleBlog} imageUrl={imageBlog} description={desBlog} onCLick={() => close()} />
                            : currentPageData.map((item) => {
                                const { datePost, description, title, id } = item;
                                const date = new Date(datePost);
                                let day = '' + date.getDate();
                                const month = getMonth(date.getMonth());
                                const year = date.getFullYear();
                                if (day.length < 2)
                                    day = '0' + day;
                                return (
                                    <li key={id}>
                                        <div>
                                            <div>
                                                <div>
                                                    <span class="month">{month} {year}</span><span class="date">{day}</span>
                                                </div>
                                                <h1><a href="/blog">{title}</a></h1>
                                            </div>
                                            <h1><a href="/blog">{title}</a></h1>
                                        </div>
                                        <p>
                                            {description.substring(0, 150)}...
                                        </p>
                                        <a onClick={() => expand(item)} style={{ cursor: 'pointer' }} class="readmore">Read More</a>
                                    </li>
                                )
                            })
                    }
                </ul>
                <ul>
                    {
                        blog.slice(0, 3).map(item => {
                            const { datePost, description, title } = item;
                            const date = new Date(datePost);
                            let day = '' + date.getDate();
                            const month = getMonth(date.getMonth());
                            const year = date.getFullYear();
                            if (day.length < 2)
                                day = '0' + day;
                            return (
                                <li>
                                    <h2><a href="/blog">{title}</a></h2>
                                    <span class="date">{day} {month} {year}</span>
                                    <p>
                                        {description.substring(0, 100)}...
                                    </p>
                                    <a href="/blog" class="readmore">Read More</a>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
            {(show === false | blog.length === 0)
                ? <ReactPaginate
                    previousLabel={"Previous"}
                    nextLabel={"Next"}
                    pageCount={pageCount}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination"}
                    previousLinkClassName={"pagination__link"}
                    nextLinkClassName={"pagination__link"}
                    disabledClassName={"pagination__link--disabled"}
                    activeClassName={"pagination__link--active"}
                />

                : <div></div>
            }
        </div>
    )
}