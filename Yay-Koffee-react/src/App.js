import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Router from "./router/Router";

function App() {
  return (
    <div id="page">
      <div>
        <Navbar />
        <Router />
        <Footer />
      </div>
    </div>
  );
}

export default App;
