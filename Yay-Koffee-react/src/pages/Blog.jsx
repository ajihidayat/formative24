import React from 'react';
import Header from '../components/Header';
import BlogComponent from '../components/BlogComponent';
import axios from 'axios';

export default class Blog extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            blog: []
        }
    }


    componentDidMount() {
        axios.get(`http://localhost:8080/section`)
            .then(res => {
                const blog = res.data;
                this.setState({ blog });
                console.log(blog);
            })
    }

    render() {
        return (
            <>
                <Header image="images/headline-blog.jpg" />
                <BlogComponent blog={this.state.blog}
                    text="" />
            </>
        );
    }
}