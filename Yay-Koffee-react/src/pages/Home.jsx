import React from 'react';
import Header from '../components/Header';
import Section from '../components/Section';
import Featured from '../components/Featured';
import axios from 'axios';

export default class Home extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            section: [],
            menu: []
        }
    }

    getSection() {
        axios.get(`http://localhost:8080/section`)
            .then(res => {
                const section = res.data;
                this.setState({ section });
                console.log(section);
            })
    }

    getMenu() {
        axios.get(`http://localhost:8080/menu`)
            .then(res => {
                const menu = res.data;
                this.setState({ menu });
                console.log(menu);
            })
    }

    componentDidMount() {
        this.getSection();
        this.getMenu();
    }

    render() {
        return (
            <>
                <Header image="images/headline-home.jpg" idHeader="home"
                    text="We are excited to introduce our Coffee Shop & Cafe." aHref="http://www.google.com"
                    aText="Find Out Why." />
                <Featured menu={this.state.menu} />
                <Section section={this.state.section} />
            </>
        );
    }
}