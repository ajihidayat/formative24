import React from 'react';
import Header from '../components/Header';
import LocationComponent from '../components/LocationComponent';
import axios from 'axios';

export default class Location extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            location: []
        }
    }

    componentDidMount() {
        axios.get(`http://localhost:8080/addressApi`)
            .then(res => {
                const location = res.data;
                this.setState({ location });
                console.log(location);
            })
    }

    render() {
        return (
            <>
                <Header image="images/headline-locations.jpg" />
                <LocationComponent location={this.state.location} />
            </>
        );
    }
}