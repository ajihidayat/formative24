import React from 'react';
import Header from '../components/Header';
import AboutUsComponent from '../components/AboutUsComponent';
import axios from 'axios';

export default class AboutUs extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            about: []
        }
    }

    componentDidMount() {
        axios.get(`http://localhost:8080/aboutApi`)
            .then(res => {
                const about = res.data;
                this.setState({ about });
                console.log(about);
            })
    }

    render() {
        return (
            <>
                <Header image="images/headline-about.jpg"
                    text="Lorem ipsum dolor sit amet." />
                <AboutUsComponent about={this.state.about} />
            </>
        );
    }
}