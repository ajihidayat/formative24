import React from 'react';
import Header from '../components/Header';
import MenuComponent from '../components/MenuComponent';
import axios from 'axios';

export default class Menu extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            menu: []
        }
    }

    componentDidMount() {
        axios.get(`http://localhost:8080/menu`)
            .then(res => {
                const menu = res.data;
                this.setState({ menu });
                console.log(menu);
            })
    }

    render() {
        return (
            <>
                <Header image="images/headline-menu.jpg"
                    text="Specials Menu YAY!KOFFEE" />
                <MenuComponent menu={this.state.menu} />
            </>
        );
    }
}