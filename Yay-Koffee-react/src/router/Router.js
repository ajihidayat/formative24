import React from 'react';
import { Route, Switch } from 'react-router';
import AboutUs from '../pages/AboutUs';
import Home from '../pages/Home';
import Blog from '../pages/Blog';
import Location from '../pages/Location';
import Menu from '../pages/Menu';

export default function Router() {
    return(
        <div id="body">
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/menu" component={Menu}/>
            <Route path="/location" component={Location}/>
            <Route path="/blog" component={Blog}/>
            <Route path="/aboutUs" component={AboutUs}/>
        </Switch>
        </div>
    )
}